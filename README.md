# Simple cli conversion program written in Rust.

### Units:
* Celcius,
* Fahrenreit,
* Kelvin.

### Possible conversion:
* all of them in between.

### Where is/are program/s
* In the target/release or target/debug subfolders.

### Release and debug?
* Release is optimized version and may not be newest version.
* Debug may not work.

### Why Rust:
* Because I want to learn it and I like it.

### How to use it:
* ```[C|K|F] degree [C|F|K]```
* ```[C|K|F] degree ```(by default it is to Celcius)

### Program accepts small and big letters, as small letters are changed to big letters. 
* C - Celcius,
* K - Kelvin,
* F - Fahrenheit,
* degree - number you want to convert.

First part describe source unit, middle is number to convert, last part is target unit of conversion.

