mod conversions;

use std::env;

//wow


fn main(){
	let help_information = "Temperature conversion help information.
	[C|K|F] degree [C|F|K] 
	[C|K|F] degree (C is defautl)
where C - Celcius, K - Kelvin, F - Fahrenreit

examples:
./temp_converter C 100 K
./temp_converter F 100 
";
	let args: Vec<String> = env::args().collect();
	let number:f32;
	let mut targetcode:String = "".to_owned();
	if args.len() > 2{
		match args.len() {
			3..=4 => {
				// C|F|K num defualt C
				// C|F|K num C|F|K

				targetcode.push_str(&args[1]);
				targetcode.make_ascii_uppercase();

				//number to convert
				number = args[2].parse().unwrap();

				//target 
				let mut target:String;
				if args.len() == 3{
					target = "C".to_string();
				}else{
					target = args[3].to_string();
				}
				target.make_ascii_uppercase();

				targetcode.push_str(target.as_str());
				targetcode = targetcode.to_string();
				//
				match targetcode.as_str() {
					// Fahrenreit to Celcius
					"FC" => {
						println!("{0:.4} F° => {1:.4} C°", number, conversions::f_to_c(number));
					}

					// Kelvin to Celcius
					"KC" => {
						println!("{0:.4} K° => {1:.4} C°", number, conversions::k_to_c(number));
					}

					// Celcius to Fahrenreit
					"CF" => {
						println!("{0:.4} C° => {1:.4} F°", number, conversions::c_to_f(number));
					}

					// Kelvin to Fahrenreit
					"KF" => {
						println!("{0:.4} K° => {1:.4} F°", number, conversions::k_to_f(number));
					}

					// Celcius to Kelvin
					"CK" => {
						println!("{0:.4} C° => {1:.4} K°", number, conversions::c_to_k(number));
					}

					// Fahrenreit to Kelvin
					"FK" => {
						println!("{0:.4} F° => {1:.4} K°", number, conversions::f_to_k(number));
					}

					"CC" | "KK" | "FF" => {
						println!("warning: there no need to convert number to the same unit.");
					}

					_ => {
						println!("Not supported option");
					}
				}
				//
			}
			_ => {
				println!("Program need to have at least 1 arguments. For more information pass -h or --help flag.");
			}
		}
	}else{
		if args[1].as_str() == "-h" || args[1].as_str() == "--help" {
			println!("{}",help_information);
		}else{
			println!("Program need to have at least 1 arguments. For more information pass -h or --help flag.");
		}
	}
}


