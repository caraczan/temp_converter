pub fn f_to_c(f:f32) -> f32{
	return (f-32.0)/1.8;
}
pub fn k_to_c(k:f32) -> f32{
	return k-273.15;
}
pub fn c_to_f(c:f32) -> f32{
	return c*1.8+32.0;
}
pub fn k_to_f(k:f32) -> f32{
	return ((k-32.0)*1.8)+273.15;
}
pub fn c_to_k(c:f32) -> f32{
	return c+273.15;
}
pub fn f_to_k(f:f32) -> f32{
	return ((f-32.0)/1.8)+273.15;
}